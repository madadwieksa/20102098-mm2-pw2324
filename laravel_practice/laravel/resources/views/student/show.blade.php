<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Biodata {{ $student->name }}</title>
</head>

<body>
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <div class="pt-3 d-flex justify-content-end align-items-center">
                    <h1 class="h2 mr-auto">Biodata {{ $student->name }}</h1>

                </div>
                <hr>
                @if (session()->has('pesan'))
                    <div class="alert alert-success">
                        {{ session()->get('pesan') }}
                    </div>
                @endif
                <ul>
                    <li>NIM: {{ $student->nim }} </li>
                    <li>Nama: {{ $student->name }} </li>
                    <li>Jenis Kelamin:
                        {{ $student->gender == 'P' ? 'Perempuan' : 'Laki-laki' }}
                    </li>
                    <li>Jurusan: {{ $student->departement }} </li>
                    <li>Alamat:
                        {{ $student->address == '' ? 'N/A' : $student->address }}
                    </li>
                    <li>
                        <td><img height="100px" src="{{ url('') }}/{{ $student->image }}"
                                class="rounded" alt=""></td>
                    </li>
                    <br>
                    <div class="d-flex justify-content">
                        <a href="{{ route('student.edit', ['student' => $student->id]) }}"
                            class="btn btn-primary mb-2 mr-2">Edit
                        </a>
                        <form action="{{ route('student.destroy', ['student' => $student->id]) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger ml-3 ml-2 mr-2">Hapus</button>
                        </form>
                        <a href="{{ route('students.index') }}" class="btn btn-secondary mb-2">Kembali</a>
                    </div>
                </ul>
            </div>
        </div>
    </div>
</body>

</html>
